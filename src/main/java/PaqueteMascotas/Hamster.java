/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

package PaqueteMascotas;

/**
 * 
 * @author Alex
 */
public class Hamster {
    
    private String nombre;
    private String color;
    private String esperanzaDeVida;
    private String peso;

    public Hamster() {
    }

    public Hamster(String nombre, String color, String esperanzaDeVida, String peso) {
        this.nombre = nombre;
        this.color = color;
        this.esperanzaDeVida = esperanzaDeVida;
        this.peso = peso;
    }

    public String getNombre() {
        return nombre;
    }

    public void setNombre(String nombre) {
        this.nombre = nombre;
    }

    public String getColor() {
        return color;
    }

    public void setColor(String color) {
        this.color = color;
    }

    public String getEsperanzaDeVida() {
        return esperanzaDeVida;
    }

    public void setEsperanzaDeVida(String esperanzaDeVida) {
        this.esperanzaDeVida = esperanzaDeVida;
    }

    public String getPeso() {
        return peso;
    }

    public void setPeso(String peso) {
        this.peso = peso;
    }
    
    

}
